import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


import {
  StackNavigator,
} from 'react-navigation';

import HomeScreen from './HomeScreen';
import ListScreen from './ListScreen';
import DetailScreenContent from './DetailScreenContent';
import DetailScreen from './DetailScreen';
import User from './User';

const App = StackNavigator({
  Home: { screen: HomeScreen },
  ListScreen: { screen: ListScreen },
  DetailScreenContent: {screen: DetailScreenContent },
  DetailScreen: {screen: DetailScreen },
  User: {screen: User}
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
