import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image,
  TextInput,
  SegmentedControlIOS,
  Switch,
  Slider,
} from 'react-native';

export default class DetailScreenContent extends React.Component {

  state = { ackeyword: '78', selectedIndex: 1 }
 
  render() {
    const { param } = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          {
            (param == 'Light1') &&
            <View style={{width: 250}}>
              <View style={styles.groupview}>
                <Text
                  style={styles.lightoneText}>Light 1
                </Text>
                <Switch  style={{width:75}}></Switch>
              </View>
              <View style={styles.groupview}>
                <Text
                  style={styles.lightoneText}>brightness
                </Text>
                <Slider style={{width:100}}></Slider>
              </View>              
            </View>
          }
          {
            (param == 'Light2') && 
            <View style={styles.groupview2}>
              <Text
                style={styles.lightoneText}>Plug
              </Text>
              <Switch></Switch>
            </View>
          }
          {
            (param == 'AC') && 
            <View style={{width: 200}}>
              <TextInput
                style={styles.acinput}
                onChangeText={(ackeyword) => this.setState({ackeyword})}
                value={this.state.ackeyword}
              />
              <SegmentedControlIOS
                values={['AC Mode', 'Fan Mode']}
                selectedIndex={this.state.selectedIndex}
                onChange={(event) => {this.setState({selectedIndex: event.nativeEvent.selectedSegmentIndex});}}
              />
            </View>
          }
          <TouchableHighlight
            activeOpacity={0.5}
            style={styles.goButton}
            onPress={this.searchProperties}
          >
            <Text style={styles.buttonText}>Schedule</Text>
          </TouchableHighlight>

          <TouchableHighlight
            activeOpacity={0.5}
            style={styles.goButton}
            onPress={this.searchProperties}
          >
            <Text style={styles.buttonText}>Historical data</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 20,
  },
  inputContainer: {
    flex: 1,
    marginTop: 50,
    alignItems: 'center',
  },
  groupview2: {
    width: 200,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 65
  },
  groupview: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  lightoneText: {
    left: 0,
    textAlign: 'center',
    color: 'gray',
  },
  acinput: {
    height: 100,
    borderRadius: 5,
    marginBottom: 40,
    marginHorizontal: 10,
    borderColor: 'gray',
    borderWidth: 1,
    textAlign: 'center',
    paddingHorizontal: 10,
  },
  goButton: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#002299',
    marginTop: 30,
  },
  buttonText: {
    color: '#002299',
  }
});
