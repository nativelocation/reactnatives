import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image,
  TextInput,
  AlertIOS,
  AsyncStorage,
} from 'react-native';


export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'BEMOSS',
  };
  state = { uname: '', pword: '', }
  
  constructor(props) {
    super(props);

    
  }

  searchProperties = () => {

    let user_pass = {
      username: 'entyd',
      password: 'LoveMe',
    }
    AsyncStorage.setItem('user_pass', JSON.stringify(user_pass), ()=>{});
    
    if ((this.state.pword == '' ) || (this.state.uname == '')) {
      AlertIOS.alert('Sync Compete', 'Input username or password correctly');
    } else {
      
      let real_info = {
        username: '',
        password: '',
      }

      AsyncStorage.getItem('real_info', (err, result)=>{
        console.log(result);

      });

      console.log(real_info);
      if (('LoveMe' == this.state.pword) & ('Chris' == this.state.uname)) {
        this.props.navigation.navigate('ListScreen'); 
      } else {
        AlertIOS.alert('Sync Compete', 'Failed username or password'); 
      }
      
    };
  }

  render() {

    const { keyword } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image source={require('./BEMOSS_logo.png')} style={{width: 260, height: 80}}/>
          <TextInput
            style={styles.input}
            onChangeText={(uname) => this.setState({uname})}
            placeholder='Username'
            placeholderTextColor='gray'
            value={this.state.uname}
          />
          <TextInput
            style={styles.input}
            onChangeText={(pword) => this.setState({pword})}
            placeholder='Password'
            placeholderTextColor='gray'
            value={this.state.pword}
          />

          <TouchableHighlight
            activeOpacity={0.5}
            style={styles.goButton}
            onPress={this.searchProperties}
          >
            <Text style={styles.buttonText}>login</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 20,
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 25,
    width: 180,
    borderRadius: 5,
    marginBottom: 5,
    borderColor: 'gray',
    borderWidth: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  goButton: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#002299',
    justifyContent: 'center',
    marginTop: 15,
  },
  buttonText: {
    color: '#002299',
  }
});
