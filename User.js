import React from 'react';
import { StyleSheet, Switch, Button, Text, View, ListView, Image } from 'react-native';



export default class User extends React.Component {
  render() {
 
    return (
      <View style={{flexDirection: 'row'}}>
        <Image style={styles.image} source={require('./1.jpg')}/>
        <Text style={styles.price}>Chris</Text>
        <Switch  style={{ flex: 1, width:75}}></Switch>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  row: {
    padding: 20,
    flexDirection: 'row',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  price: {
     flex: 1,
  },
  image: {
    //width: 40,
    height: 100,
    flex: 1,
  }
});

