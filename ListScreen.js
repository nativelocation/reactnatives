import React from 'react';
import { StyleSheet, Text, View, Button, ListView, Image, TouchableHighlight } from 'react-native';
import { DrawerNavigator } from 'react-navigation';
import HomeScreen from './HomeScreen';
import DetailScreenContent from './DetailScreenContent';
import DetailScreen from './DetailScreen';
import User from './User';

export class ListItemScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'BEMOSS',
      headerRight:(
      <Button
        title={'Menu'}
        onPress={() => navigation.navigate('DrawerOpen')}
        style={{paddingRight: 10}}
      />
    )};
  };
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    this.state = {
      dataSource: this.ds.cloneWithRows(['Light1', 'Light2', 'AC']),
    };
  }

  renderRow = (rowData) => {
    // console.log("ListScreen props renderRow : " + this.navigate);

    const { navigate } = this.props.navigation;
    return (
      <View>
        <TouchableHighlight
          style={styles.row}
          onPress={() => navigate('DetailScreenContent', {param: rowData,})}
        >
          <Text style={styles.part}>{rowData}</Text>
        </TouchableHighlight>
      </View>
    );
  }

  render() {
    const { dataSource } = this.state;
    return (
      <View style={styles.container}>
        {dataSource &&
          <ListView
            dataSource={dataSource}
            renderRow={this.renderRow}
            enableEmptySections
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  row: {
    paddingLeft: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  rowImage: {
    width: 50,
    height: 50,
    marginRight: 20,
  },
  part: {
    //color: 'blue',
    fontSize: 20,
    fontWeight: 'bold',
  }
});

const Username = ({navigation, param}) => (
  <User />
);

const Logout = ({navigation, param}) => (
  <HomeScreen />
);

const HVAC = ({navigation, param}) => (
  <DetailScreen param={'AC'} navigation={navigation} />
);

const Lighting = ({ navigation }) => (
  <DetailScreen param={'Light1'} navigation={navigation} />
);

const Plug = ({ navigation }) => (
  <DetailScreen param={'Light2'} navigation={navigation} />
);

const List = ({ navigation }) => (
  <ListItemScreen param={'Light2'} navigation={navigation} />
);


const ListScreen = DrawerNavigator(
  {
    Username: {
      path: '/User',
      screen: User,
    },
    HVAC: {
      path: '/DetailScreen',
      screen: DetailScreen,
    },
    Lighting: {
      path: '/DetailScreen',
      screen: DetailScreen,
    },
    Plug: {
      path: '/DetailScreen',
      screen: DetailScreen,
    },
    Logout: {
      path: '/',
      screen: HomeScreen,
    },
    List: {
      path: '/ListItemScreen',
      screen: ListItemScreen,
    },
  },
  {
    initialRouteName: 'List',
  }
);

export default ListScreen;
